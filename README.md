# ETAVM

Este projeto destina-se a manter uma tecla do teclado pressionada sempre que o processo configurado pelo programador estiver sendo executado. 
A melhor aplicabilidade deste projeto é para execução de testes automatizados em maquinas virtuais.