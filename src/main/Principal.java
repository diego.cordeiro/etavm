package main;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Principal {

	public static void main(String[] args) throws InterruptedException, IOException, AWTException {
		Robot r = new Robot();
		
		System.out.println("Iniciando Verifica��o de Processo");

		while (verificaExecucaoProcesso("Winium.Desktop.Driver.exe")) {
			r.keyPress(KeyEvent.VK_PAUSE);
			r.delay(1000);
		}

	}

	public static boolean verificaExecucaoProcesso(String nomeProcesso) throws InterruptedException, IOException {
		String line = "";
		String pidInfo = "";
		boolean retorno = false;

		try {
			Process p = Runtime.getRuntime().exec(System.getenv("windir") + "\\system32\\" + "tasklist.exe");
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = input.readLine()) != null) {
				pidInfo += line;
				// System.out.println("Processo sendo executado: " + line);
			}

			if (pidInfo.contains(nomeProcesso)) {
				//System.out.println("O processo " + nomeProcesso + ", est� em execu��o");
				retorno = true;
			} else {
				System.out.println("O processo " + nomeProcesso + ", n�o est� em execu��o");
			}

			input.close();
		} catch (Exception e) {
			System.out.println("Saida de erro: " + e);
		}

		return retorno;
	}

}
